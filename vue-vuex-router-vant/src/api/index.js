/**
 * Created by shooke on 17-7-19.
 */
// export const baseUrl = 'http://www.boxun100.com/zhuzuoquan/service/public/index.php/'
// export const staticUrl = 'http://www.boxun100.com/zhuzuoquan/service/public/static/'
// export const socketUrl = 'ws://localhost:9501'
import {baseUrl, staticUrl} from './local'
import Vue from 'vue'
import Qs from 'qs'
import Axios from 'axios'
import VueAxios from 'vue-axios'

let axios = Axios.create({
  baseURL: baseUrl,
  transformRequest: [function (data) {
    data = Qs.stringify(data)
    return data
  }],
  // params: {
  //   access_token: store.getters.token
  // },
  headers: {'Content-Type': 'application/x-www-form-urlencoded'}
})
Vue.use(VueAxios, axios)

// 基础地址和静态文件地址
export {baseUrl, staticUrl}

export function login (data) {
  let url = 'web/visitor/login'
  return axios.post(url, data)
}

export const planList = 'fuchanpin/plan/list'
export const planGet = 'fuchanpin/plan/get'
export const planAdd = 'fuchanpin/plan/add'
export const planEdit = 'fuchanpin/plan/edit'

export const topicList = 'fuchanpin/topic/list'
export const topicGet = 'fuchanpin/topic/get'
export const topicAdd = 'fuchanpin/topic/add'
export const topicEdit = 'fuchanpin/topic/edit'
