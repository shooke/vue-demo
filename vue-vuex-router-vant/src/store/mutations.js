/**
 * Created by shooke on 17-7-19.
 */
export const setUser = function (state, user) {
  // 保存user
  window.sessionStorage.setItem('user', JSON.stringify(user))
  // 设置user
  state.user = user
}
export const setToken = function (state, token) {
  // 保存token和过期时间
  window.sessionStorage.setItem('token', token)
  // 设置token和过期时间
  state.token = token
}
