/**
 * Created by shooke on 17-7-19.
 */

export const user = function (state) {
  if (state.user === null) {
    state.user = JSON.parse(window.sessionStorage.getItem('user'))
  }
  return state.user
}

export const token = function (state) {
  if (state.token === '') {
    state.token = window.sessionStorage.getItem('token')
  }
  return state.token ? state.token : ''
}
