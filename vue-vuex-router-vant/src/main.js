// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import FastClick from 'fastclick'
import App from './App'
import router from './router'
import store from './store'

// 载入axios
import './api'

import Vant from 'vant'
import 'vant/lib/vant-css/index.css'
Vue.use(Vant)

// 路由控制 登录状态
// router.beforeEach((to, from, next) => {
//   if (to.path !== '/' && store.state.token === '') {
//     next({
//       path: '/'
//     })
//   }
//   next()
// })

FastClick.attach(document.body)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
