/**
 * Created by Administrator on 2017/7/18 0018.
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import {GRAY_BODY, WHITE_BODY} from 'components/api/index-web';
import goods from 'components/goods/Goods';
import foodDetail from 'components/fooddetail/FoodDetail';
import order from 'components/order/Order';
import orderCenter from 'components/ordercenter/OrderCenter';
import ratings from 'components/ratings/Ratings';
import refund from 'components/refund/Refund';
import allOrders from 'components/allorders/AllOrders';
import stores from 'components/store/Stores';
import confirmPay from 'components/pay/ConfirmPay';
import pay from 'components/pay/Pay';
import cards from 'components/cards/Cards';
import demand from 'components/demand/Demand';
import search from 'components/search/Search';
import searchDetail from 'components/searchdetail/SearchDetail';
import regist from 'components/regist/Regist';
import address from 'components/address/Address';
import addressAdd from 'components/address/AddressAdd';
import addressEdit from 'components/address/AddressEdit';
import distribution from 'components/distribution/Distribution';
import payResult from 'components/pay/PayResult';
import errorPage from 'components/common/ErrorPage';
import orderDetail from 'components/orderdetail/OrderDetail';
import timeList from 'components/timelist/TimeList';
import noteInfo from 'components/note/NoteInfo';
import navigation from 'components/store/Navigation';
import store from './store.config';

Vue.use(VueRouter);

export default new VueRouter({
  routes: [{
    path: '/goods',
    name: 'goods',
    component: goods,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = WHITE_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/search',
    component: search,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = WHITE_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    },
    meta: {
      keep: true
    }
  }, {
    name: 'searchDetail',
    path: '/searchDetail',
    component: searchDetail,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = WHITE_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    },
    meta: {
      keep: true
    }
  }, {
    path: '/demand',
    component: demand,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = WHITE_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/cards',
    component: cards,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    },
    meta: {
      keep: true
    }
  }, {
    path: '/foodDetail',
    component: foodDetail,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = WHITE_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/regist',
    component: regist,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/address',
    component: address,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/addressAdd',
    component: addressAdd,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/addressEdit',
    component: addressEdit,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/distribution',
    name: 'distribution',
    component: distribution,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/pay/:orderId',
    name: 'pay',
    component: pay,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = '#f2f5fa';
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/payResult/:orderId/:branchId',
    name: 'payResult',
    component: payResult,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = '#f2f5fa';
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/order',
    name: 'order',
    component: order,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/orderCenter',
    component: orderCenter,
    children: [{
      path: '/',
      redirect: 'allOrders'
    }, {
      path: 'allOrders',
      name: 'allOrders',
      component: allOrders,
      beforeEnter(to, from, next) {
        window.document.body.style.backgroundColor = GRAY_BODY;
        next();
      },
      beforeLeave(to, from, next) {
        window.document.body.style.backgroundColor = '';
        next();
      }
    }, {
      path: 'ratings',
      component: ratings,
      beforeEnter(to, from, next) {
        window.document.body.style.backgroundColor = GRAY_BODY;
        next();
      },
      beforeLeave(to, from, next) {
        window.document.body.style.backgroundColor = '';
        next();
      }
    }, {
      path: 'refund',
      component: refund,
      beforeEnter(to, from, next) {
        window.document.body.style.backgroundColor = GRAY_BODY;
        next();
      },
      beforeLeave(to, from, next) {
        window.document.body.style.backgroundColor = '';
        next();
      }
    }],
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/orderDetail/:orderId/:branchId',
    name: 'orderDetail',
    component: orderDetail,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/noteInfo',
    component: noteInfo,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    },
    meta: {
      keep: true
    }
  }, {
    path: '/stores',
    component: stores
  }, {
    path: '/timeList',
    component: timeList
  }, {
    path: '/confirmPay',
    component: confirmPay,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/error',
    name: 'error',
    component: errorPage,
    beforeEnter(to, from, next) {
      window.document.body.style.backgroundColor = GRAY_BODY;
      next();
    },
    beforeLeave(to, from, next) {
      window.document.body.style.backgroundColor = '';
      next();
    }
  }, {
    path: '/navigation',
    component: navigation
  }, {
    path: '*',
    redirect: '/goods'
  }],
  linkActiveClass: 'active'
});
