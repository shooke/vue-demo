/**
 * Created by wd on 2017/8/23.
 */
import Vue from 'vue'
import Validator from 'vue-validator'
Vue.use(Validator);
Vue.validator('text', function (val) {
  return /^\d+$/.test(val)
});
