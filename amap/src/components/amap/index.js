import Vue from 'vue';
import VueAMap, {lazyAMapApiLoaderInstance} from 'vue-amap';


import amapKey from './config';
import amapAddress from './AmapAddress';
import amapNavigation from './AmapNavigation'

Vue.use(VueAMap);
VueAMap.initAMapApiLoader({
  key: amapKey,
  plugin: ['Autocomplete', 'PlaceSearch', 'Scale', 'OverView', 'ToolBar', 'MapType', 'PolyEditor', 'AMap.CircleEditor']
});

/**
 * 将路径选择和地点选取注册为全局组件
 */
Vue.component('amap-address', amapAddress);
Vue.component('amap-navigation', amapNavigation);


/**
 * 用于使用原生高德对象
 * 使用方式 this.
 * this.$amapLoad().then(() => {
 *   console.log(AMap)
 * })
 */
Vue.prototype.$amapLoad = function () {
  return lazyAMapApiLoaderInstance.load()
}
