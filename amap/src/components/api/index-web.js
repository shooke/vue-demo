export const GRAY_BODY = '#efefef';
export const WHITE_BODY = '#FFF';

// 菜品要求是否多选
export const CAN_MULTI = 'YES';
export const CAN_NOT_MULTI = 'NO';

// 菜品要求是否必选
export const CAN_MUST = 'YES';
export const CAN_NOT_MUST = 'NO';

// 首次下单是否需要验证手机号
export const CAN_CHECK = 'YES';
export const CAN_NOT_CHECK = 'NO';

// 是否默认地址
export const DEFALUT_ADDRESS = 'YES';
export const NOT_DEFAULT_ADDRESS = 'NO';
// 字数限制
export const WORDS_LIMIT = 50;
// 验证码倒计时
export const TIME_COUNT = 60;

// 接口自定义code状态
export const RESPONSE_CODE_SUCCESS = 'SUCCESS';
export const RESPONSE_CODE_FAIL = 'FAIL';
