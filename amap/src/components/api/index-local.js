/**
 * 全局常量定义
 * @author ytl
 */

// local 本地 ；server服务器；打包时，请切换到server模式
export const APP_ENV = 'local';

// saas接口地址
export const INTERACE_SAAS_BASE_URL = 'http://www.hocyun.cn:9100/v1';

// 好会员接口地址
export const INTERACE_HHY_BASE_URL = 'http://crm.hocyun.cn/api-app/v1';

// 商户中心接口
export const INTERACE_MERCHANT_CENTER_BASE_URL = 'http://www.hocyun.cn:8889/v1';

// saas订单接口地址
export const INTERACE_SAAS_ORDER_BASE_URL = 'http://www.hocyun.cn:9200/v1';   // 'http://www.hocyun.cn:9200/v1';

export const MEMBER_RECHARGE_URL = 'http://crm.hocyun.cn/weixinMarketing/frontend/web/index.php/member/pay/member-recharge?';