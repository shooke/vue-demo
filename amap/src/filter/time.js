import Vue from 'vue';
import moment from 'moment';
/**
 * @todo 普通日期格式化
 * @author wangd
 */
Vue.filter('time', function (value, formatString) {
  return moment(value).format(formatString);
});

/**
 * @todo 将时间戳转换成日期时间，然后格式化
 * @author wangd
 * @param timestamp 时间戳
 * @return yyyy-MM-dd hh:mm:ss
 */
Vue.filter('formatTime', function (timestamp) {
  let date = '';
  if (timestamp) {
    date = new Date(parseInt(timestamp) * 1000);
  } else {
    date = new Date();
  }
  let Y = date.getFullYear(),
    m = date.getMonth() + 1,
    d = date.getDate(),
    H = date.getHours(),
    i = date.getMinutes(),
    s = date.getSeconds();
  if (m < 10) {
    m = '0' + m;
  }
  if (d < 10) {
    d = '0' + d;
  }
  if (H < 10) {
    H = '0' + H;
  }
  if (i < 10) {
    i = '0' + i;
  }
  if (s < 10) {
    s = '0' + s;
  }
  let tt = Y + '-' + m + '-' + d + ' ' + H + ':' + i;
  return tt;
});

/**
 * @todo 格式化金额
 * @author wangd
 * @param 如果金额为整数，则直接显示￥金额，如果为小数，则显示保留两位小数的人民币金额
 * @return 例：￥12 || ￥12.26
 */
Vue.filter('money', function (value) {
  let reg = /\./;
  if (reg.test(value)) {
    let fixedNum = value.toString().split('.')[1];//
    if (fixedNum === '00') {
      return '￥' + value.toString().split('.')[0];
    } else {
      if (fixedNum.length > 1) {
        if (fixedNum[fixedNum.length - 1] === '0') {
          return '￥' + parseFloat(value).toFixed(1);
        } else {
          return '￥' + parseFloat(value).toFixed(2);
        }
      } else {
        return '￥' + parseFloat(value).toFixed(1);
      }
    }
  } else {
    return '￥' + value;
  }
});
Vue.filter('money1', function (value) {
  let reg = /\./;
  if (!value) {
    return '-￥0';
  }
  if (reg.test(value)) {
    let fixedNum = value.toString().split('.')[1];//
    if (fixedNum === '00') {
      return '-￥' + value.toString().split('.')[0];
    } else {
      return '-￥' + parseFloat(value).toFixed(2);
    }
  } else {
    return '-￥' + value;
  }
});
