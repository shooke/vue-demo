/**
 * Created by shooke on 17-7-19.
 */
import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';
import * as actions from './actions';
import * as mutations from './mutations';

Vue.use(Vuex);

const state = {
  user: null,
  token: 'token'
};

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
});
