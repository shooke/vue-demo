import Home from "@/views/Home.vue";
import store from "@/store";

const routers = [
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/login.vue")
    // beforeEnter: (to, from, next) => {
    //   if (store.getters.getAuth != "") {
    //     next("/home");
    //   }
    //   next();
    // }
  },
  {
    path: "/home",
    name: "home",
    component: Home
  },
  {
    path: "/todo/:uid",
    name: "todo",
    component: () => import("@/views/todo.vue")
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("@/views/About.vue")
  },
  {
    path: "/slot",
    name: "slot",
    component: () => import("@/views/slot.vue")
  }
];
export default routers;
