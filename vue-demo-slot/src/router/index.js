import Vue from "vue";
import VueRouter from "vue-router";
import routesArr from "./routers-arr";
import store from "@/store";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: routesArr
});

router.beforeEach((to, from, next) => {
  console.log(to, from, store.getters.getAuth);
  if (store.getters.getAuth == "" && to.path != "/login") {
    next("/login");
    return;
  }
  next();
});
export default router;
