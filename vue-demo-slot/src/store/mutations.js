export default {
  setUsername(state, value) {
    window.localStorage.setItem("username", value);
    state.username = value;
  },
  setAuth(state, value) {
    window.localStorage.setItem("auth", value);
    state.auth = value;
  }
};
