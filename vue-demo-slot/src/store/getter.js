export function getUsername(state) {
  let localuser = window.localStorage.getItem("username");
  if (localuser) {
    state.username = localuser;
  }
  return state.username;
}
export function getAuth(state) {
  let localuser = window.localStorage.getItem("auth");
  if (localuser) {
    state.auth = localuser;
  }
  return state.auth;
}
