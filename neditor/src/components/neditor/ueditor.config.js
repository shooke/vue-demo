export default {
  UEDITOR_HOME_URL: process.env.BASE_URL + 'neditor/',
  // 编辑器不自动被内容撑高
  autoHeightEnabled: false,
  // 初始容器高度
  initialFrameHeight: 350,
  // 初始容器宽度
  initialFrameWidth: '100%',
  catchRemoteImageEnable: false,
  imageMaxSize: 10248000, // 上传大小限制，单位B
  zIndex: 9999,
  // 关闭自动保存
  enableAutoSave: false,
  // serverUrl: 'http://localhost/php/controller.php'
  serverUrl: '',

  imageUploadService: function (context, editor) {
    return {
      /** 
       * 触发fileQueued事件时执行
       * 当文件被加入队列以后触发，用来设置上传相关的数据 (比如: url和自定义参数)
       * @param {Object} file 当前选择的文件对象
       */
      setUploadData: function (file) {
        return file;
      },
      /**
       * 触发uploadBeforeSend事件时执行
       * 在文件上传之前触发，用来添加附带参数
       * @param {Object} object 当前上传对象
       * @param {Object} data 默认的上传参数，可以扩展此对象来控制上传参数
       * @param {Object} headers 可以扩展此对象来控制上传头部
       * @returns 上传参数对象
       */
      setFormData: function (object, data, headers) {
        return data;
      },
      /**
       * 触发startUpload事件时执行
       * 当开始上传流程时触发，用来设置Uploader配置项
       * @param {Object} uploader
       * @returns uploader
       */
      setUploaderOptions: function (uploader) {
        return uploader;
      },
      /**
       * 触发uploadSuccess事件时执行
       * 当文件上传成功时触发，可以在这里修改上传接口返回的response对象
       * @param {Object} res 上传接口返回的response
       * @returns {Boolean} 上传接口返回的response成功状态条件 (比如: res.code == 200)
       */
      getResponseSuccess: function (res) {
        return res.code == 200;
      },
      /* 指定上传接口返回的response中图片路径的字段，默认为 url
       * 如果图片路径字段不是res的属性，可以写成 对象.属性 的方式，例如：data.url 
       * */
      imageSrcField: 'url'
    }
  },

  /**
   * 视频上传service
   * @param {Object} context UploadVideo对象 视频上传上下文
   * @param {Object} editor  编辑器对象
   * @returns videoUploadService 对象
   */
  videoUploadService: function (context, editor) {
    return {
      /** 
       * 触发fileQueued事件时执行
       * 当文件被加入队列以后触发，用来设置上传相关的数据 (比如: url和自定义参数)
       * @param {Object} file 当前选择的文件对象
       */
      setUploadData: function (file) {
        return file;
      },
      /**
       * 触发uploadBeforeSend事件时执行
       * 在文件上传之前触发，用来添加附带参数
       * @param {Object} object 当前上传对象
       * @param {Object} data 默认的上传参数，可以扩展此对象来控制上传参数
       * @param {Object} headers 可以扩展此对象来控制上传头部
       * @returns 上传参数对象
       */
      setFormData: function (object, data, headers) {
        return data;
      },
      /**
       * 触发startUpload事件时执行
       * 当开始上传流程时触发，用来设置Uploader配置项
       * @param {Object} uploader
       * @returns uploader
       */
      setUploaderOptions: function (uploader) {
        return uploader;
      },
      /**
       * 触发uploadSuccess事件时执行
       * 当文件上传成功时触发，可以在这里修改上传接口返回的response对象
       * @param {Object} res 上传接口返回的response
       * @returns {Boolean} 上传接口返回的response成功状态条件 (比如: res.code == 200)
       */
      getResponseSuccess: function (res) {
        return res.code == 200;
      },
      /* 指定上传接口返回的response中视频路径的字段，默认为 url
       * 如果视频路径字段不是res的属性，可以写成 对象.属性 的方式，例如：data.url 
       * */
      videoSrcField: 'url'
    }
  }
}
