import Vue from 'vue';
import Router from 'vue-router';

import Login from '@/components/login';

import fundsList from '@/components/funds/list';
import fundsAdd from '@/components/funds/add';
import fundsEdit from '@/components/funds/edit';

import departmentList from '@/components/department/list';
import departmentAdd from '@/components/department/add';
import departmentEdit from '@/components/department/edit';

Vue.use(Router);

export default new Router({
  routes: [

    {
      path: '/login',
      component: Login
    },
    {
      path: '/funds/list/:page',
      component: fundsList
    },
    {
      path: '/funds/add',
      component: fundsAdd
    },
    {
      path: '/funds/edit/:id',
      component: fundsEdit
    },
    {
      path: '/department/list/:page',
      component: departmentList
    },
    {
      path: '/department/add',
      component: departmentAdd
    },
    {
      path: '/department/edit/:id',
      component: departmentEdit
    }
  ]
});
