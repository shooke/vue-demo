// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-default/index.css';
Vue.use(ElementUI);

// 载入axios
import './api';

Vue.config.productionTip = false;

// 路由控制 登录状态
router.beforeEach((to, from, next) => {
  if (to.path !== '/login' && store.state.token === '') {
    next({
      path: '/login'
    });
  }
  next();
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: {App}
});
